$(function() {
	
	$('.btn-enterprise').on('click', function() {
		$(this).addClass('dropicon');
		var id = $(this).attr('aria-controls');
		if($('.'+id).hasClass(id+'_active')){
			$('.dropicon i').removeClass('rotate');
			$('.'+id).removeClass(id+'_active');
			$('.'+id).addClass('desaturate');
		} else {
			$('.'+id).addClass(id+'_active');
			$('.dropicon i').addClass('rotate');
			$('.'+id).removeClass('desaturate');
		}
	});

	$('.btn-modal').on('click', function() {
		$('#eventos').addClass('modal-none');
		$('#convidados').removeClass('modal-none');
	});
	$('#back').on('click', function() {
		$('#eventos').removeClass('modal-none');
		$('#convidados').addClass('modal-none');
	});

	$('.close-menu').on('click', function() {
		if($('.navbar-collapse').hasClass('show')) {
			$('.navbar-collapse').removeClass('show');
		}
	});

	resizeSlider();
	function resizeSlider() {
		var box = $('.box-content-internal').height();
		if(box > 0) {
			$('.bg-full').each(function() {
				var style = $(this).attr('style');
				style += 'height:'+box+'px;';
				$(this).attr('style', style);
			});

		}
	}

	var height = $('.contact-info').height();
	height += 20;
	$('.form-contact').attr('style', 'height:'+height+'px');

	$('.btn-modal').on('click', function() {
		id = $(this).attr("id");
		var modal = document.getElementById('openvideo-'+id);
		var btn = document.getElementById(id);
		var span = document.getElementsByClassName("close-"+id)[0];
		modal.style.display = "block";
		span.onclick = function() {
			modal.style.display = "none";
		}
		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
			}
		}
	});

});